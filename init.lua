minetest.register_chatcommand("export", {
	params = "",
	description = "Exports the currently running nodeset",
	privs = {server = true},
	func = function(name, params)
		if not minetest.check_player_privs(name, "server") then
			return false
		end

		local sanatized_defs = {}

		for name, def in pairs(minetest.registered_nodes) do
			if name ~= "air" and name ~= "ignore" and name ~= "" then
				sanatized_defs[name] = {
					description = def.description,
					drawtype = def.drawtype,
					mesh = def.mesh,
					tiles = def.tiles and table.copy(def.tiles),
					node_box = def.node_box and table.copy(def.node_box),
					paramtype2 = def.paramtype2,
					groups = {oddly_breakable_by_hand = 1, cracky = 1},
				}
			end
		end

		local export = ([=[
			local defs = minetest.deserialize([[%s]])

			for name, def in pairs(defs) do
				minetest.register_node(":"..name, def)
			end
		]=]):format(minetest.serialize(sanatized_defs))

		local worlddir = minetest.get_worldpath()
		local file = io.open(worlddir .. "/init.lua", "w+")
		if not file then
			minetest.chat_send_player(name, "Failed to open file")
			return
		end

		file:write(export)
		file:close()

		minetest.chat_send_player(name, "Export successful!")
		return true
	end,
})