# Node Exporter

This mod adds a chatcommand which will export all currently registered nodes. It will place an `init.lua` file in the base directory of your world, which you can then copy into a new mod. Included is also a tiny shell script which will copy all of the PNG and OBJ files found in the directory given to it into the current directory.

## Usage

Do something like this:

1. Run your main world with this mod loaded
2. `/export`
3. Copy `worlddir/init.lua` to `mods/my_dummynodes/init.lua`
4. Create `mods/my_dummynodes/media`
5. `cd` into `mods/my_dummynodes/media` and run `bash ~/.minetest/mods/exporter/import_media.sh ~/.minetest/games/mygame/`

You can now use this mod in another game, like `void` with `schemedit` and an inventory mod.

## License

Everything here is CC0.